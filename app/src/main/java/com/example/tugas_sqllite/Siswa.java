package com.example.tugas_sqllite;

public class Siswa {
    int noSiswa;
    String nama;
    String date;
    String jenisKelamin;
    String Alamat;

    public int getNoSiswa() {
        return noSiswa;
    }

    public void setNoSiswa(int noSiswa) {
        this.noSiswa = noSiswa;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getAlamat() {
        return Alamat;
    }

    public void setAlamat(String alamat) {
        Alamat = alamat;
    }


}
